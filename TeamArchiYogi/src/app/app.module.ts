import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import {AppRoutingModule,routingComponents} from './app-routing.module';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { IndexComponent } from './index/index.component';

import {ProjectTypeService} from './project-type.service';
//import { WhyTeamYogiComponent } from './why-team-yogi/why-team-yogi.component';
//import { HomeComponent } from './home/home.component';
//import { AboutUsComponent } from './about-us/about-us.component';
//import { ContactComponent } from './contact/contact.component';
//import { ProjectsComponent } from './projects/projects.component';

@NgModule({
  declarations: [
    AppComponent,
    routingComponents,
    HeaderComponent,
    FooterComponent,
    IndexComponent
    
       
  ],

  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpModule
  ],
  providers: [ProjectTypeService],
  bootstrap: [AppComponent]
})
export class AppModule { }
