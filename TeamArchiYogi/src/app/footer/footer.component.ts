import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  title = 'Copyrights "2018" TeamArchiyogi || Designed by Team!';
  constructor() { }

  ngOnInit() {
  }

}
