import {NgModule} from '@angular/core';
import {Routes,RouterModule} from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ContactComponent } from './contact/contact.component';
import { ProjectsComponent } from './projects/projects.component';
import { AboutUsComponent } from './about-us/about-us.component';
import {WhyTeamYogiComponent} from './why-team-yogi/why-team-yogi.component';

const routes: Routes =[
    { path:'',redirectTo:'\home',pathMatch:'full'},
    { path:'home',component:HomeComponent},
    { path:'contactus',component:ContactComponent},
    { path:'projects',component:ProjectsComponent},
    { path:'aboutus',component:AboutUsComponent},
    { path:'why-Team',component:WhyTeamYogiComponent},
];

@NgModule({
    imports:[RouterModule.forRoot(routes)],
    exports:[RouterModule]
})
export class AppRoutingModule{}
export const routingComponents=[HomeComponent,ContactComponent,ProjectsComponent,AboutUsComponent,WhyTeamYogiComponent]