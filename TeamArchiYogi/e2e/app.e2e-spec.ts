import { TeamArchiYogiPage } from './app.po';

describe('team-archi-yogi App', function() {
  let page: TeamArchiYogiPage;

  beforeEach(() => {
    page = new TeamArchiYogiPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
