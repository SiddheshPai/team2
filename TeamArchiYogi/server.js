const express = require ('express');
const bodyParser = require ('body-parser');
const path = require('path');
const http = require('http');
const app = express();

// Api File For Interacting With MongoDB
const api = require('./server/routes/api');

//Parsers
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// Angular DIST Output Folder
app.use(express.start(path.join(__dirname,'dist')));

//Api Location
app.use('/api',api);

//Send all other request to the Angular Application
app.get('*',(req,res)=>{
    res.sendFile(path.join(__dirname,'dist/index.html'));
});

//Set Port
const port = process.env.PORT ||'3000';
app.set('port',port);

const server = http.createServer(app);

server.listen(port,()=>console.log('Running On Localhost : ${port}'));
